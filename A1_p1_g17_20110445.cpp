#include <iostream>
#include <string>

using namespace std ; 

class BigDecimalInt {
	public : 
	int num [100] ;
	int size ;
	BigDecimalInt () {}
	
	BigDecimalInt (int a, int s){
		size =s ;
		for(int i=(s-1);i>=0;i--){
	     num[i]= a %10;
	      a =a /10 ;
	    }
	  
	}

	BigDecimalInt (string a){
		size=a.length();
	 	for(int i=0; i<a.length();i++){
	 		num[i]=a[i]-'0';
		 }
	}
	BigDecimalInt operator+(const BigDecimalInt&) ;
     
  
 friend ostream& operator << (ostream& out, const BigDecimalInt& m)
  {
      out << m.num << endl;	
	   return out ;
	   }
   
}; 
	
BigDecimalInt BigDecimalInt::operator+(const BigDecimalInt& parameter){
	BigDecimalInt temp ;
	temp.size = size ;
	for(int i=0;i<size;i++){
	  temp.num[i] = num[i] + parameter.num[i] ;	
	} 
		return temp ;
}

int main (){
	
	BigDecimalInt big1("123456789123456789123456789"); 
	BigDecimalInt big2("123456789123456789123456789");
	BigDecimalInt big3 ; 
	big3 = big1 + big2 ;
	
  /*BigDecimalInt big1(123456789,9); 
	BigDecimalInt big2(123456789,9);
	BigDecimalInt big3 ; 
	big3 = big1 + big2 ;
	*/
	cout<<"result is ";
	for(int i=0;i<big3.size;i++)
	cout<<big3.num[i];
	cout<<endl;
	 
	return 0 ;
}
